#include "sram_rng.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "mbedtls/config.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/entropy.h"
#include "mbedtls/sha256.h"
#include "mbedtls/sha512.h"

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

// Option B
//volatile uint8_t sram[RAM_SIZE] __attribute__((section (".noinit")));

int isInitialized = 0;
uint8_t entropy[ENTROPY_LENGTH];
int entropyCounter = 0;

mbedtls_entropy_context ctxEntropy;
mbedtls_ctr_drbg_context ctxDrbg;

int entropySource(void *data, unsigned char *output, size_t len, size_t *olen)
{
	if (entropyCounter >= ENTROPY_LENGTH)
	{
		return MBEDTLS_ERR_ENTROPY_SOURCE_FAILED;
	}
	if (len + entropyCounter > ENTROPY_LENGTH)
	{
		len = ENTROPY_LENGTH - entropyCounter;
	}

	memcpy(output, (unsigned char*)(entropy+entropyCounter), len);
	entropyCounter += len;

	*olen = len;
	return 0;
}

int SRNG_ExportSRAM()
{
	printf("\rStartADR: \r\n%x\n", RAM_START_ADR);
	printf("\rExportSRAM:\r\n");

	for (int i = 0; i < (RAM_END_ADR-RAM_START_ADR) / 4; i++)
	{
		printf("%08x", *(unsigned int*)(RAM_START_ADR+i));
	}

	printf("\r\n");

	return 0;
}

int SRNG_ExportRandomValues()
{
	if (!isInitialized) return -1;

	int cnt = 0, res = 0, i = 0;
	uint8_t random[16];

	res = SRNG_GetRandom(random, 16);
	for (; !res; cnt+=16)
	{
		for (i = 0; i < 16; i++) printf(BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(random[i]));
		res = SRNG_GetRandom(random, 16);
	}

	return cnt;
}

int SRNG_CollectEntropy()
{
	mbedtls_ctr_drbg_init(&ctxDrbg);
	mbedtls_entropy_init(&ctxEntropy);
	if (mbedtls_entropy_add_source(&ctxEntropy, entropySource, NULL, 1, 1) != 0) return -1;
	if (mbedtls_ctr_drbg_seed(&ctxDrbg, mbedtls_entropy_func, &ctxEntropy, NULL, 0) != 0) return -1;

	int (*sha)(const unsigned char*, size_t, unsigned char*, int) = &HASH_FUNC;

	for (int i = 0; i < RAM_BLOCK_COUNT && (i*HASH_LENGTH) < ENTROPY_LENGTH; i++)
	{
		if((*sha)((uint8_t*)(FREE_RAM_START_ADR + i*(RAM_BLOCK_SIZE / 4)), RAM_BLOCK_SIZE, (uint8_t*)(entropy + i*HASH_LENGTH), 0) != 0) return -1;
	}

	isInitialized = 1;
	return 0;
}

int SRNG_GetRandom(unsigned char* output, size_t olen)
{
	if (!isInitialized) return -1;
	if (olen <= 16) return mbedtls_ctr_drbg_random(&ctxDrbg, output, olen);

	int len = 0, res = 0;
	for (int i = 0; i < olen && !res; i += len)
	{
		len = (olen-i) > 16 ? 16 : (olen-i);
		res = mbedtls_ctr_drbg_random(&ctxDrbg, (unsigned char*)(output + i), len);
	}

	return res;
}

void SRNG_FreeRandom()
{
	if (isInitialized)
	{
		mbedtls_ctr_drbg_free(&ctxDrbg);
		mbedtls_entropy_free(&ctxEntropy);
	}
}
