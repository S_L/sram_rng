#ifndef INC_SRAM_RNG_H_
#define INC_SRAM_RNG_H_

#include <stddef.h>

// Option A) search in entire SRAM for free memory
#define RAM_START_ADR 	0x20000000
#define RAM_END_ADR 	0x20018000
// Option B) set the address explicit
//#define RAM_SIZE (32*1024)
//#define RAM_START_ADR ((unsigned int)&sram)
//#define RAM_END_ADR (RAM_START_ADR+RAM_SIZE)
// To evaluate your implementation, use the following defines
//#define RAM_START_ADR 	FREE_RAM_START_ADR
//#define RAM_END_ADR 	FREE_RAM_EMD_ADR

// Replace the following lines provided by the python tool
#define FREE_RAM_START_ADR 0x20012bc0
#define FREE_RAM_EMD_ADR 0x20018000
#define RAM_BLOCK_SIZE (3840/8)
#define RAM_BLOCK_COUNT 44 // (int)((FREE_RAM_END_ADR-FREE_RAM_START_ADR)/RAM_BLOCK_SIZE)


#define ENTROPY_LENGTH (RAM_BLOCK_COUNT * HASH_LENGTH) // can be changed to less (not more)

#define USE_SHA256
//#define USE_SHA512

#if defined(USE_SHA256)
#define HASH_LENGTH 32
#define HASH_FUNC mbedtls_sha256_ret
#elif defined(USE_SHA512)
#define HASH_LENGTH 64
#define HASH_FUNC mbedtls_sha512_ret
#endif

/**
 * \brief          Prints current RAM values on console.
 * 				   The output starts with RAM start address
 * 				   and is followed by the values in hex format.
 *
 * \return         \c 0 on success.
 */
int SRNG_ExportSRAM();

/**
 * \brief          Prints all available random values on console.
 * 				   The output is in binary format.
 *
 * \return         \c 0 on success.
 * \return         A negative error code on failure.
 */
int SRNG_ExportRandomValues();

/**
 * \brief          Initializes the SRAM Random Number Generator.
 * 				   This must be done as one of the first things on system startup.
 * 				   First, RAM is read, then hashed and last stored in an entropy array.
 * 				   The entropy array is used as seed source for the CTR-DRBG.
 *
 * \return         \c 0 on success.
 * \return         A negative error code on failure.
 */
int SRNG_CollectEntropy();

/**
 * \brief          Gets random values. Note: this source is limited.
 * 				   Precisely, count of random values AND calls of this function are limited.
 * 				   To get the maximum bytes out of this source, request multiples of \c 16.
 *
 * \param output   The random data. This must be a writable buffer of given length.
 * \param olen     The length of output array.
 *
 * \return         \c 0 on success.
 * \return         A negative error code on failure.
 */
int SRNG_GetRandom(unsigned char* output, size_t olen);

/**
 * \brief          This function clears the required mbedtls entropy and drbg contexts.
 */
void SRNG_FreeRandom();

#endif /* INC_SRAM_RNG_H_ */
