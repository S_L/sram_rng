**SRAM Random Number Generator (RNG) for Microcontrollers**

True random values are very important in the field of security. But not all microcontrollers have a hardware True Random Number Generator (TRNG). Therefore, a solution for IoT devices, which definitely need security mechanisms, is needed. This project seeks to generate "somewhat true" random values by exploiting physical processes in SRAM cells. When the microcontroller is powered on, the state of a SRAM cell may not be the same on every start, so sometimes 0, sometimes 1 (but the majority remains the same). Please have a look on this [blog](https://blog.usejournal.com/how-to-generate-true-random-numbers-in-a-microcontroller-b0c65903ae84) for further information.

This project is a practical approach. To comply with standards, I recommend:
- [SP 800-90B: Recommendation for the Entropy Sources Used for Random Bit Generation](https://csrc.nist.gov/publications/detail/sp/800-90b/final)
- [SP 800-22 Rev. 1a: A Statistical Test Suite for Random and Pseudorandom Number Generators for Cryptographic Applications](https://csrc.nist.gov/publications/detail/sp/800-22/rev-1a/final)

**Abstract Method**

1. Read the SRAM startup values.
2. Compress the values by using a hash function. (Used in this project: SHA256)
3. Use the hashes as a seed for a Cryptographically Secure Pseudorandom Number Generator (CSPRNG). (Used in this project: CTR-DRBG)

**Limitations**

Using this method, only a limited number of "somewhat true" random values can be generated. Devices requiring many secure random values or devices that get rarely powered off need other sources, such as human input.

**Scope of this Project**

- This repository contains a STM32 Cube IDE project for a STM32 L4 controller (the controller can be easily exchanged).
- The SRAM values can be exported to a PC using a UART communication.
- The SRAM values can be analyzed by a python script to verify the randomness of the values. The output of this analysis is important to generate correct seeds for a given entropy. 
- The library can be used to generate a buffer of "somewhat true" random values on system start.

**Requirements**

- Any IDE. I used STM Cube IDE, a conversion to a other IDE should not take too long.
- Python 3. I used Python 3.6.9
- (Linux; some extra work may be required for other systems)

**Project Setup**

1. Clone or download this repository
2. µC project setup: exchange the controller or create a new project. Steps for a new project: 
    1. Create a new project for your controller. The configuration is mainly not important except a UART connection. I use USART2.  
    2. Copy the following header files in folder `"repo"/SRAM_RNG/Core/Inc` to your new project `Inc` folder: `mbedtls` (entire folder), `retarget.h`, `sram_rng.h`
    3. Copy the following source files in folder `"repo"/SRAM_RNG/Core/Src` to your new project `Src` folder: `library` (entire folder), `retarget.c`, `sram_rng.c`
    4. Copy the includes and user code in `"repo"/SRAM_RNG/Core/Src/main.c` to your new project `main.c` file.
    5. printf output is directed to UART via `retarget.c/h`. Exchange the include file for your µC. This may be different on non STM controllers. 
    6. (For autogenerated projects) Exclude `syscalls.c` from your build (or delete it).
3. Build the project.

**Project Configuration**

The address of the SRAM to analyze must be defined. This is done in sram_rng.h. There are two options.
- Option A)
    - Define the start and end address of the entire RAM. The python tool will look for memory that is suitable for our intent.
    - Exchange `#define RAM_START_ADR` and `#define RAM_END_ADR`.
- Option B)
    - We need to create an array that will not be initialized (the bigger the better).
    - Define an uninitialized section in the linker script (`*Flash.ld`).
```  
  /* Creating a non initialized RAM section: */
  .noinit (NOLOAD):
  {
    KEEP(*(.noinit)) /* keep my variable even if not referenced */
  } >RAM
```
- Still Option B)
    - In `sram_rng.c`, uncomment the array for Option B.
    - In `sram_rng.h`, comment the define for Option A, uncomment the defines for Option B.
    - Set the size of your RAM array in `#define RAM_SIZE`.

**Project Usage**

When we run the project, the RAM values are printed. By opening putty or something similar, we can see the values on the PC. We need to save these values so that we can compare them to the new values after a power off and on.
On Linux we can do that automatically with the following command: `(stty raw; cat > sramXX.txt) < /dev/ttyACM0`
Change `ttyACMO` to your controller. The following steps need to be done several times (it takes a bit time). For testing, 5 times is enough. For release, many more a needed.

Setup:
1. Connect the µC to your PC.
2. You may have to change the access rights: `cd /dev; sudo chmod 666 ./ttyACM0`
3. cd `"repo"/Evaluation`

Loop:
1. `(stty raw; cat > sramXX.txt) < /dev/ttyACM0`
2. Start the project in your IDE.
3. On finish, stop the program, stop command 1 and rename the file to `sram01.txt`. (Increase the name, order is important)
4. Switch the power of the µC completely off. This is important so that the RAM cells get in a new state. Connect the µC again. Repeat these steps now.

Start the python script `"repo"/sram_eval.py`. It reads the previously created sramXX.txt files and evaluates the RAM. Follow the steps in the script.