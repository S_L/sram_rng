import os
import sys

from math import *
from statistics import *
import textwrap

filePath = './Evaluation'
fileNameBase = 'sram'

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def printStatistics(array):
    print('Min      : ' + str(min(array)))
    print('Max      : ' + str(max(array)))
    print('Median   : ' + str(median(array)))
    print('Mean     : ' + str(mean(array)))
    print('Std      : ' + str(stdev(array)))

def getFiles():
    files = [f for f in os.listdir(filePath) if os.path.isfile(os.path.join(filePath,f)) and fileNameBase in f]
    files = sorted(files)
    correctFiles = []
    for f in files:
        with open(os.path.join(filePath,f), 'r') as reader:
            lines = reader.readlines()
            lines.reverse()
            starts = list(filter(lambda x: x == 'ExportSRAM:\n', lines))
            if starts and len(starts) > 0:
                data = lines[lines.index(starts[0])-1]
                correctFiles.append([f, len(data), data])
    return correctFiles

def inputNumeric(message, defaultValue, format = 10):
    if format == 2: val = bin(defaultValue)
    elif format == 16: val = hex(defaultValue)
    else: val = str(defaultValue) 

    print(message + ' [Default ' + val + ']:')
    reqEntropy = defaultValue
    while True:
        ent = input()
        if ent.isnumeric():
            reqEntropy = int(ent)
            break
        elif ent.startswith('0x'):
            reqEntropy = int(ent, 16)
            break
        elif ent.startswith('0b'):
            reqEntropy = int(ent, 2)
            break
        elif ent == '':
            break
        else:
            print('Invalid input, type again:')
    return reqEntropy

def evalSRAMcomplete(correctFiles):
    percentages = []
    changedBits = 0
    expectedBitLen = correctFiles[0][1] * 4

    # identify areas that did not change (initialized areas)
    areaSize = 64*2 # use 64 bytes
    initializedAreas = '0'*ceil(expectedBitLen/areaSize)
    for i in range(0, len(correctFiles)-1):
        areasCurr = textwrap.wrap(correctFiles[i][2], areaSize)
        areasNext = textwrap.wrap(correctFiles[i+1][2], areaSize)
        for k in range(0, len(areasCurr)):
            xor =  bin(int(areasCurr[k], 16) ^ int(areasNext[k], 16))[2:]
            if (xor.count('1') / areaSize) < 0.01:
                initializedAreas = initializedAreas[:k] + '1' + initializedAreas[k+1:]

    # find longest, uninitialized section
    seqStarted = False
    seqLen = 0
    maxLen = 0
    maxLenIndex = 0
    for i in range(0, len(initializedAreas)):
        if initializedAreas[i] == '0':
            if not seqStarted: 
                seqStarted = True
                seqLen = 1
            else: seqLen = seqLen + 1
        else:
            if seqStarted: 
                seqStarted = False
                if seqLen > maxLen:
                    maxLen = seqLen
                    maxLenIndex = i - seqLen
    if seqStarted and seqLen > maxLen:
        maxLen = seqLen
        maxLenIndex = len(initializedAreas) - seqLen

    # trim the data files
    offset = 0
    if maxLenIndex > 0:
        offset = maxLenIndex*areaSize
        for f in correctFiles:
            f[2] = f[2][offset:(offset+maxLen*areaSize)]
            f[1] = len(f[2])
        expectedBitLen = correctFiles[0][1] * 4

    for i in range(0, len(correctFiles)-1):
        currVal = correctFiles[i][2]
        nextVal = correctFiles[i+1][2]

        xor = int(currVal, 16) ^ int(nextVal, 16)
        changedBits = changedBits | xor
        percentages.append(bin(xor).count('1') / expectedBitLen)
    
    printStatistics(percentages)
    print('Static   : ' + str((bin(changedBits).count('0')-1) / expectedBitLen))
    return (correctFiles, offset, percentages, expectedBitLen)

def evalSRAMblocks(correctFiles, expectedLen, blockSize, reqEntropy):
    blocks = [[] for x in range(ceil(expectedLen / blockSize))]
    passed = [0] * ceil(expectedLen / blockSize)
    failed = [0] * ceil(expectedLen / blockSize)
    ratio =  [0] * ceil(expectedLen / blockSize)
    
    for i in range(0, len(correctFiles)-1):
        xor = bin(int(correctFiles[i][2], 16) ^ int(correctFiles[i+1][2], 16))[2:]
        bs = textwrap.wrap(xor, blockSize)
        for i in range(0, len(bs)):
            blocks[i].append(bs[i].count('1'))
    
    if (expectedLen / blockSize) != 0:
        blocks = blocks[:-1] # remove last which is not full size
        passed = passed[:-1]
        failed = failed[:-1]
        ratio = ratio[:-1]

    for i in range(0, len(blocks)):
        passed[i] = len(list(filter(lambda x: x > reqEntropy, blocks[i])))
        failed[i] = len(blocks[i]) - passed[i]
        ratio[i] = passed[i] / (passed[i] + failed[i])

    return (blocks, passed, failed, ratio)

def main():
    print('Change window size to maximum to see the results correctly.\nPress enter to start...')
    input()
    correctFiles = getFiles() # get SRAM Export files
    if len(correctFiles) > 0:
        length = correctFiles[0][1]
        if any(x[1] != correctFiles[0][1] for x in correctFiles):
            print('Data is not the same length, smallest set is used')
            length = min(map(lambda x: x[1], correctFiles))
            for f in correctFiles:
                f[1] = length
                f[2] = f[2][:length]
            print(correctFiles)
            print('\n')

        startADR = 0x10000000
        with open(os.path.join(filePath, correctFiles[0][0]), 'r') as reader:
            while True:
                line = reader.readline()
                if line == 'StartADR: \n':
                    startADR = int(reader.readline(), 16)
                    break
                elif line == '':
                    break

        startADR = inputNumeric('SRAM start address?', startADR, 16) # address required for #defines
        reqEntropy = inputNumeric('Required bits of entropy?', 128) # flipping bits per block, since some bits never flip
        
        print('1) Evaluation of bits in entire SRAM that changed between the power on and offs. Result in percent:')
        resReal = evalSRAMcomplete(correctFiles)
        correctFiles = resReal[0]
        percentages = resReal[2]
        expectedBitLen = resReal[3]
        if resReal[1] > 0: # found "free" memory has an offset
            newStartADR = int(startADR + resReal[1]/2)
            print('Found initalized memory at ' + hex(startADR) + ', new start address: ' + hex(newStartADR))
            startADR = newStartADR
        endADR = int(startADR + expectedBitLen/8) # end address might not be the RAM end address

        try:
            realBit =  int(1 / (floor(min(percentages) * 100) / 100))
        except:
            print('The memory is not good enough. Provided data cannot be used as RNG')
            print('Finished with errors')
            return
        
        print('--> ' + str(realBit) + ' bits in SRAM are about 1 bit entropy')

        blockSizeReal = realBit * reqEntropy
        print('This results in a block size of ' + str(blockSizeReal) + ' bits\n')

        print('2) Evaluation of bits within a SRAM block to check whether each block fulfills the requirement:')
        blockSizeReal = inputNumeric('Evaluate minimum block size?', blockSizeReal, 10)
        recBit = realBit
        resReal = evalSRAMblocks(correctFiles, expectedBitLen, blockSizeReal, reqEntropy) # result for minimum blocks
        resRec = None # result for recommened blocks
        blocksReal = resReal[0]
        allPassed = len(list(filter(lambda x: x <= 0.99, resReal[3]))) == 0
        while allPassed == False: # find a block size so that all blocks fulfill the entropy requirement
            recBit = recBit + 1
            blockSizeRec = recBit * reqEntropy
            resRec = evalSRAMblocks(correctFiles, expectedBitLen, blockSizeRec, reqEntropy)
            allPassed = len(list(filter(lambda x: x <= 0.99, resRec[3]))) == 0
            if recBit == realBit + 20:
                break

        # print result of this evaluation
        print('Minimum block size: ' + str(blockSizeReal) + ' (' + str(len(blocksReal)) + ' blocks)')
        if resRec != None:
            print('Recommended block size: ' + str(blockSizeRec) + ' (' + str(len(resRec[0])) + ' blocks)')
        header = 'No.\tAddress \tResult Block Size = ' + str(blockSizeReal)
        if resRec != None:
            header = header + 32*' ' + 'Address \tBlock Size = ' + str(blockSizeRec)
        print(header)

        for i in range(0, len(blocksReal)):
            passed = len(list(filter(lambda x: x > reqEntropy, blocksReal[i])))
            failed = len(blocksReal[i]) - passed
            ratio = passed / (passed + failed)
            passedClr = bcolors.OKGREEN if (passed > 0 and failed == 0) else bcolors.ENDC
            failedClr = bcolors.FAIL if (failed > 0) else bcolors.ENDC
            ratioClr = bcolors.OKGREEN if ratio >= 0.99 else (bcolors.WARNING if ratio > 0.65 else bcolors.FAIL)
            line = str(i).rjust(3) + '\t' + hex(startADR + int(i*(blockSizeReal/8))) + '\t' + passedClr + 'PASSED: ' + str(passed).rjust(3) + '\t' + failedClr + 'FAILED: ' + str(failed).rjust(3) + '\t' + ratioClr + 'RATIO: ' + str(round(ratio, 2)) + bcolors.ENDC
            if resRec != None and i < len(resRec[3]):
                ratio = resRec[3][i]
                ratioClr = bcolors.OKGREEN if ratio == 1 else (bcolors.WARNING if ratio > 0.65 else bcolors.FAIL)
                line = line + '\t|\t' + hex(startADR + int(i*(blockSizeRec/8))) + '\t' + ratioClr + 'RATIO: ' + str(round(ratio, 2)) + bcolors.ENDC
            print(line)

        if resRec != None:
            print('\nReplace the defines in sram_rng.h by one of the following lines:')
            print('Option A) (not fully secure!) allows to generate a maximum of ' + str(int(len(resReal[0])*32/128)*80000) + ' random bytes based on SRAM')
            print('#define FREE_RAM_START_ADR ' + hex(startADR))
            print('#define FREE_RAM_EMD_ADR ' + hex(endADR))
            print('#define RAM_BLOCK_SIZE ' + str(blockSizeReal))
            print('#define RAM_BLOCK_COUNT ' + str(len(resReal[0])) + ' // (int)((FREE_RAM_END_ADR-FREE_RAM_START_ADR)/RAM_BLOCK_SIZE)')
            print('Option B) (recommended!) allows to generate a maximum of ' + str(int(len(resRec[0])*32/128)*80000) + ' random bytes based on SRAM')
            print('#define FREE_RAM_START_ADR ' + hex(startADR))
            print('#define FREE_RAM_EMD_ADR ' + hex(endADR))
            print('#define RAM_BLOCK_SIZE ' + str(blockSizeRec))
            print('#define RAM_BLOCK_COUNT ' + str(len(resRec[0])) + ' // (int)((FREE_RAM_END_ADR-FREE_RAM_START_ADR)/RAM_BLOCK_SIZE)')
        else:
            print('\nReplace the defines in sram_rng.h by the following lines:')
            print('This allows to generate a maximum of ' + str(int(len(resReal[0])*32/128)*80000) + ' random bytes based on SRAM')
            print('#define FREE_RAM_START_ADR ' + hex(startADR))
            print('#define FREE_RAM_EMD_ADR ' + hex(endADR))
            print('#define RAM_BLOCK_SIZE ' + str(blockSizeReal))
            print('#define RAM_BLOCK_COUNT ' + str(len(resReal[0])) + ' // (int)((FREE_RAM_END_ADR-FREE_RAM_START_ADR)/RAM_BLOCK_SIZE)')

        print('\nFinished')
    else:
        print('No files found!')


if __name__ == "__main__":
    main()